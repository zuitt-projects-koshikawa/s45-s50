import React, {useState, useContext} from 'react';
import { Navbar, Nav } from 'react-bootstrap';
import {Link} from 'react-router-dom';
/*v5: NavLink, Link*/
import UserContext from '../UserContext';

export default function AppNavBar(){

  const {user} = useContext(UserContext);
  console.log(user)

  // const [user, setUser] = useState(localStorage.getItem("email"));
  // console.log(user)

  return(
    
    <Navbar bg="light" expand="lg">
        <Navbar.Brand as={Link} to="/" className="m-3">Zuitt</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="ml-auto">
            <Nav.Link as={Link} to="/">Home</Nav.Link>
            <Nav.Link as={Link} to="/courses">Courses</Nav.Link>

           { (user.id !== null) ?

            	<Nav.Link as={Link} to="/logout">Logout</Nav.Link>
            :
            <>
            	<Nav.Link as={Link} to="/register">Register</Nav.Link>
           		<Nav.Link as={Link} to="/login">Log In</Nav.Link>
            </>
          }
          </Nav>
        </Navbar.Collapse>
    </Navbar>

  )
}
