import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
// To use bootstrap this is needed
import 'bootstrap/dist/css/bootstrap.min.css'

// Should only contain 1 component
ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);


// const name = "Tomio Koshikawa";

// const user ={
//   firstName: "Chris",
//   lastName: "Pratt"
// }

// const formatName = (user) =>{
//   return `${user.firstName} ${user.lastName}`
// }

// // JSX 
// const element = <h1>Hello my name is, {formatName(user)}</h1>

// ReactDOM.render(
//     element,
//     document.getElementById("root")
//   );

