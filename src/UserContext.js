import React from 'react';

// Creates a content object
	// Context object as the name states is a  data type of an object that can be used  to store information that can be shared to other component within app.
	// The context object is different approach to passing  information between components and allows us for easier access by avoiding the use of prop drilling

const UserContext = React.createContext();

	// The "Provider" component allows other components to consume  the context object and supply the necessary information needed to context object -CONSUME

export const UserProvider = UserContext.Provider;

export default UserContext;