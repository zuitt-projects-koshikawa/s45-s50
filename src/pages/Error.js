
import Banner  from '../components/Banner'

export default function Error () {
		
		const data = {

			title: "404: Page Not Found",
			content: "Area You Lost",
			destination: "/",
			label: "Back to Homepage"
	}
	return(

		<Banner data={data}/>

	)
}