// import
import { useState, useEffect, useContext } from 'react';
import {Navigate} from 'react-router-dom';
import {Form, Button} from 'react-bootstrap';
import Swal from 'sweetalert2'
import UserContext from '../UserContext';

export default function Login(props) {

	// Allows us to consume the User Context object and its properties to use for user validation.		
	const {user, setUser} = useContext(UserContext);
	console.log(user)

	const [email, setEmail] = useState('') 
	const [password, setPassword] = useState('')
	const [isActive, setIsActive] = useState(true)

	// console.log(email)
	// console.log(password)

	function authenticate(e){
		e.preventDefault();

		fetch('http://localhost:4000/users/login',{
			method:"POST",
			headers: {
					"Content-Type" : "application/json"
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
			
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(typeof data.access !== "undefined") {
				localStorage.setItem ('token', data.access)
				retrieveUserDetails(data.access)

				Swal.fire({
					title: 'Login Successful',
					icon: 'success',
					text: 'Welcome to Zuitt!'
				})

			} else {

				Swal.fire({
					title: 'Authentication Failed',
					icon: 'error',
					text: 'Please check your credentials'
				})
			}
		})

		// set the email of the authenticated user in the local storage

		// Syntax: localStorage.setitem ("key", value)
		// localStorage.setItem("email", email)

		// to access the user information, it can be done using  localStorage,  this is necessary to update  the user state which will help update the App component and rerender it to avoid refreshing the page upon user login and logout.
		// setUser({
		// 	email: localStorage.getItem('email')
		// }) 


		// Clear input fields
		setEmail('');
		setPassword('');




		const retrieveUserDetails = (token) => {
			fetch('http://localhost:4000/users/details',{
				method: "POST",
				headers: {
					Authorization: `Bearer ${token}`
				}
			})
			.then(res => res.json())
			.then(data => {
				console.log(data)

				setUser({
					id: data._id,
					isAdmin: data.isAdmin
				})
			})

		}
		// alert('You have succesfully Logged In!');
	}
	
	//Hooks
	useEffect(() => {
		if(email !== '' && password !== '') {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [email, password])

	return(

		(user.id !== null) ?
			<Navigate to= '/courses'/> 
			
		:

			<Form onSubmit= {e => authenticate(e)} >
				<h1>Log In</h1>
				<Form.Group controlId="email">
					<Form.Label>Email Address</Form.Label>
					<Form.Control
						type="email"
						placeholder="Enter your email here"
						value={email}
						onChange= {e => setEmail(e.target.value)}
						required
					/>
				</Form.Group>

				<Form.Group controlId="password">
					<Form.Label>Password</Form.Label>
					<Form.Control
						type="password"
						placeholder="Input your password here"
						value={password}
						onChange={e => setPassword(e.target.value)}
						required
					/>
					{isActive ? 
						<Button variant="primary" type="submit" id="submitBtn" className="my-3">	
							Log In
						</Button> 
						: 
						<Button variant="danger" type="submit" id="submitBtn" className="my-3" disabled>
							Log In
						</Button>
					}

				</Form.Group>
			</Form>

	)
}