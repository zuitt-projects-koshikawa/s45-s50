import {useContext, useEffect} from 'react';
import {Navigate} from 'react-router-dom';
// v5: Redirect to
import UserContext from '../UserContext'
// When we logout we want the local storage to be cleared

export default function Logout() {

	const {unsetUser, setUser} = useContext(UserContext);

	// clears the localStorage of the user's infromation
	unsetUser()
	// localStorage.clear()
	// localStorage.clear() is already declared in app.js

	useEffect(() => {
		// set the user state back to its original value
		setUser({id: null})
	}, [])

	return (
		<Navigate to='/login'/>

	)
}